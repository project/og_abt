NAME
------------------------
Organic Groups - Audience by Content Type


DESCRIPTION
------------------------
This module modified the node edit form's audience selector, provided by the Organic Groups module, such that the groups are selectable by content type.  This is useful on sites with a very large number of groups and many different group types.  For example, a site with Districts, Schools and Interest Groups (all content types defined as organic groups) will have an individual selector for each group type.


INSTALLATION
------------------------
Just enable the module, and you're all set.  There are no configuration options.


CREDITS
------------------------
Authored and maintained by Jeff Beeman (jrbeeman on drupal.org)